variable "name"            { default = "hstack-net" }
variable "region"          { default = "us-east-1" }

variable "vpc_cidr"        { default = "10.100.0.0/16" }

variable "availability_zones" { default = "us-east-1a,us-east-1b,us-east-1c"}

variable "atlas_username"  { default = "hstack" }
variable "hstack_version"  { default = "1.0" }

variable "bastion_count"   { default = "1" }
variable "bastion_instance_type" { default = "t2.micro" }

variable "nat_count" { default = "-1" } # -1 will create as many nat instances as number of az provided

variable "keypair_name"        { }
