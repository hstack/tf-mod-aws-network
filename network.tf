#---------------------------------------------------------------
# This module creates all networking resources for hstack in aws
#---------------------------------------------------------------

module "vpc" {
  source            = "./vpc"

  name = "${var.name}-vpc"
  cidr = "${var.vpc_cidr}"
}

module "public_subnet" {
  source            = "./public_subnet"

  name                  = "${var.name}-public"
  vpc_id                = "${module.vpc.vpc_id}"
  vpc_cidr              = "${var.vpc_cidr}"
  availability_zones    = "${var.availability_zones}"
}

module "nat" {
  source            = "./nat"

  name              = "${var.name}-nat"
  count             = "${length(slice(split(",",var.availability_zones), 0, (var.nat_count < 0 ? length(split(",",var.availability_zones)) : var.nat_count)))}"

  public_subnet_ids = "${join(",", slice(split(",",module.public_subnet.subnet_ids), 0, (var.nat_count < 0 ? length(split(",",module.public_subnet.subnet_ids)) : var.nat_count)))}"
}

module "private_subnet" {
  source            = "./private_subnet"

  name   = "${var.name}-private"
  vpc_id = "${module.vpc.vpc_id}"
  availability_zones    = "${var.availability_zones}"

  nat_gateway_ids = "${module.nat.nat_gateway_ids}"
}

module "bastion" {
  source             = "./bastion"
  count              = "${var.bastion_count}"
  name               = "${var.name}-bastion"
  hstack_version     = "${var.hstack_version}"
  atlas_username     = "${var.atlas_username}"
  availability_zones = "${var.availability_zones}"
  vpc_id             = "${module.vpc.vpc_id}"
  region             = "${var.region}"
  public_subnet_ids  = "${module.public_subnet.subnet_ids}"
  keypair_name       = "${var.keypair_name}"
  instance_type      = "${var.bastion_instance_type}"
}

resource "aws_network_acl" "acl" {
  vpc_id     = "${module.vpc.vpc_id}"
  subnet_ids = ["${concat(split(",", module.public_subnet.subnet_ids), split(",", module.private_subnet.subnet_ids))}"]

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags { Name = "${var.name}" }
}
