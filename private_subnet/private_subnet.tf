#--------------------------------------------------------------
# This module creates all resources necessary for a private
# subnet
#--------------------------------------------------------------

variable "name"               { default = "hstack-private"}
variable "vpc_id"             {}

variable "block_size"         { default = "4" }

variable "availability_zones" { }
variable "nat_gateway_ids"    { }

data "aws_vpc" "selected" {
  id = "${var.vpc_id}"
}

resource "aws_subnet" "private" {
  vpc_id            = "${var.vpc_id}"
  cidr_block        = "${cidrsubnet(data.aws_vpc.selected.cidr_block, var.block_size, (count.index * 2 + (count.index*2) % 2 + 1))}"
  availability_zone = "${element(split(",", var.availability_zones), count.index)}"
  count             = "${length(split(",", var.availability_zones))}"

  tags      { Name = "${var.name}.${element(split(",", var.availability_zones), count.index)}" }
  lifecycle { create_before_destroy = true }
}

resource "aws_route_table" "private" {
  vpc_id = "${var.vpc_id}"
  count  = "${length(split(",", var.availability_zones))}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${element(split(",", var.nat_gateway_ids), count.index)}"
  }

  tags      { Name = "${var.name}.${element(split(",", var.availability_zones), count.index)}" }
  lifecycle { create_before_destroy = true }
}

resource "aws_route_table_association" "private" {
  count          = "${length(split(",", var.availability_zones))}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"

  lifecycle { create_before_destroy = true }
}

output "subnet_ids" { value = "${join(",", aws_subnet.private.*.id)}" }
