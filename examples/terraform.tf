variable "aws_region" { default = "us-east-1" }
variable "bastion_count" { default = "3" }
variable "hstack_version" { default = "1" }
variable "keypair_name" { }

provider "aws" {
  region = "${var.aws_region}"

}

module "net" {
   source = "git::https://gitlab.com/hstack/tf-mod-aws-network.git"
   bastion_count = "${var.bastion_count}"
   keypair_name = "${var.keypair_name}"
   hstack_version = "${var.hstack_version}"
   nat_count = "1"
}

output "configuration" {
  value = <<CONFIGURATION

* Add your private key and SSH into any private node via the Bastion host:
  $$ ssh-add path_to_private_sshkey
  $$ ssh -A core@${module.net.bastion_public_dns}

* Add these lines to your ~/.ssh/config file to connect to your instances through the bastion:
   ---
   Host "${replace(replace(module.net.vpc_cidr,".0",""),"/\\/[\\d]+/",".*")}"
      IdentityFile path_to_private_sshkey
      User core
      ProxyCommand ssh -l core -i path_to_private_sshkey -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null "${module.net.bastion_public_dns}" ncat %h %p
   ---
CONFIGURATION
}
