#--------------------------------------------------------------
# This module creates all resources necessary for a Bastion
# host
#--------------------------------------------------------------

variable "name"              { default = "hstack-bastion" }
variable "count"          { default = "1" }
variable "user"              { default = "core" }
variable "atlas_username"    { default = "hstack"}
variable "hstack_version"    { default = "1.0" }
variable "availability_zones" { }
variable "vpc_id"            { }
variable "region"            { }
variable "public_subnet_ids" { }
variable "keypair_name"      { default = "" }

variable "instance_type"     { }

resource "aws_security_group" "bastion" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix        = "${var.name}"
  vpc_id      = "${var.vpc_id}"
  description = "Bastion security group (only SSH inbound access is allowed)"

  tags {
    Name = "${var.name}"
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  lifecycle {
    create_before_destroy = true
  }
}

data "atlas_artifact" "bastion_ami" {
  name = "${var.atlas_username}/aws-${var.region}-coreos-hstack"

  type = "amazon.image"
  metadata {
      version = "${var.hstack_version}"
  }
}

resource "aws_launch_configuration" "bastion" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix          = "${var.name}"
  image_id             = "${lookup(data.atlas_artifact.bastion_ami.metadata_full, format("region-%s", var.region))}"
  instance_type        = "${var.instance_type}"
  security_groups      = ["${aws_security_group.bastion.id}"]

  associate_public_ip_address = "false"
  key_name                = "${var.keypair_name}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "bastion" {
  count =  "${var.count> 0 ? 1 : 0}"

  name                      = "${var.name}"
  vpc_zone_identifier       = [ "${split(",",var.public_subnet_ids)}"]
  desired_capacity          = "${var.count}"
  min_size                  = "${var.count}"
  max_size                  = "${var.count}"
  health_check_grace_period = "60"
  health_check_type         = "EC2"
  force_delete              = false
  wait_for_capacity_timeout = 0
  launch_configuration      = "${aws_launch_configuration.bastion.name}"
  enabled_metrics           = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]

  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "bastion" {
  count =  "${var.count> 0 ? 1 : 0}"

  name = "${var.name}-elb"

  subnets = ["${split(",", var.public_subnet_ids)}"]

  listener {
    instance_port = 22
    instance_protocol = "tcp"
    lb_port = 22
    lb_protocol = "tcp"
  }

  security_groups      = ["${aws_security_group.bastion.id}"]

  cross_zone_load_balancing = true
  idle_timeout = 400
  connection_draining = true
  connection_draining_timeout = 400

  tags {
    Name = "${var.name}"
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_bastion" {
  count =  "${var.count> 0 ? 1 : 0}"

  autoscaling_group_name = "${aws_autoscaling_group.bastion.id}"
  elb                    = "${aws_elb.bastion.id}"
}

output "elb_dns"           { value = "${var.count> 0 ? aws_elb.bastion.dns_name : "no bastion setup requested" }" }
